<?php
session_start();
session_unset();
session_destroy();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>logout</title>
  <?php include 'css/css.html'; ?>
</head>

<body>
    <div class="form">
          <h1>bedankt voor je taak!</h1>
          <p><?= 'je bent uitgelogt!'; ?></p>
          <a href="index.php"><button class="button button-block"/>Home</button></a>
    </div>
</body>
</html>
