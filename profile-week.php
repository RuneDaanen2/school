<?php
session_start();

if ( $_SESSION['logged_in'] != 1 ) {
    $_SESSION['message'] = "je moet ingelogt zijn";
    header("location: error.php");
}
else {
    $user_name = $_SESSION['user_name'];
}
include_once 'system/db.php';
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title><?= $user_name ?></title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <?php include 'css/css.html'; ?>
</head>
    <body>
        <div class="form">
            <div class="dropdown">
                <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Menu</button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="profile.php">Home</a>
                    <a class="dropdown-item disabled" >Week overzicht</a>
                    <a class="dropdown-item" href="logout-sure.php">Uitloggen</a>
                </div>
            </div>
            <h2>Week overzicht</h2>
            <br>
            <div class="accordion" id="accordionExample">
                <?php
                $i = 1;
                for ($i = -2; $i < 3; $i++) {
                    $taskDate = date('Y-m-d', strtotime($i . ' days', time()));
                    $result = $mysqli->query("SELECT * FROM tasks WHERE task_date = '$taskDate'");
                    $ResultUpdate = date('l d-m ', strtotime($i . ' days', time()));
                    $ChangeLanguage = str_replace(array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'), array('Maandag', 'Dinsdag', 'Woensdag','Donderdag','Vrijdag','Zaterdag','Zondag'), $ResultUpdate);
                    if ($i == 0) {
                        $bgcolor = 'style="background-color:#1ab188;"';
                        $color = 'style="color:white;"';
                    } else {
                        $bgcolor = '';
                        $color = 'style="color:black;"';
                    }
                ?>
                <div class="card" <?php if (!empty($bgcolor)) {echo $bgcolor;} ?>>
                    <div class="card-header" id="heading<?php echo $i ?>">
                            <button class="btn btn-link"  type="button" data-toggle="collapse" data-target="#collapse<?php echo $i  ?>" aria-expanded="true" aria-controls="collapse<?php echo $i ?>">
                                <h5 <?php echo $color ?> class="mb-0">
                                <?php
                                    echo $ChangeLanguage;
                                ?>
                                </h5>
                            </button>
                    </div>
                    <div id="collapse<?php echo $i  ?>" class="collapse" aria-labelledby="heading<?php echo $i  ?>" data-parent="#accordionExample">
                        <div class="card-body">
                            <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Naam</th>
                                    <th scope="col">Taak naam</th>
                                </tr>
                            </thead>
                            <?php
                            while($row = $result->fetch_array())
                            if (!empty($row['jobs_job_name'])) {
                                if ($user_name == $row['users_user_name']){ ?>
                                    <tr>
                                        <th style="color: #C91D2B;"><?php echo $row['users_user_name'] ?></th>
                                        <td style="color: #C91D2B;"><?php echo $row['jobs_job_name'] ?></td>
                                    </tr>
                                <?php } else {
                                    ?>
                                    <tr>
                                        <th><?php echo $row['users_user_name'] ?></th>
                                        <td><?php echo $row['jobs_job_name'] ?></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </table>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
        <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
        <script src="bootstrap/js/bootstrap.bundle.js" charset="utf-8"></script>
        <script src="js/index.js"></script>
    </body>
</html>
