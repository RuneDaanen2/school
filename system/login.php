<?php
session_start();

$username = $mysqli->escape_string($_POST['username']);
$result = $mysqli->query("SELECT * FROM users WHERE user_name ='$username'");

if ( $result->num_rows == 0 ){
    $_SESSION['message'] = "User with that name doesn't exist!";
    header("location: error.php");
}
else {
    $user = $result->fetch_assoc();

    if ( password_verify($_POST['password'], $user['user_pass']) ) {
        if ($user['user_rank'] != '0') {

            $_SESSION['user_name'] = $user['user_name'];
            $_SESSION['logged_in-admin'] = true;

            header("location: admin/week-view.php");
        } else {
            $_SESSION['user_name'] = $user['user_name'];
            $_SESSION['logged_in'] = true;

            header("location: profile.php");
        }
    }
    else {
        $_SESSION['message'] = "You have entered wrong password, try again!";
        header("location: error.php");
    }
}
