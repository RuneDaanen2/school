<?php
include_once 'system/db.php';
session_start();

if ( $_SESSION['logged_in'] != 1 ) {
  $_SESSION['message'] = "je moet ingelogt zijn";
  header("location: error.php");
}
else {
    $user_name = $_SESSION['user_name'];
}
$taskDate = date('Y-m-d', strtotime('0 days',time()));
$result = $mysqli->query("SELECT * FROM tasks WHERE task_date = '$taskDate' and users_user_name = '$user_name'");

?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title><?= $user_name ?></title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <?php include 'css/css.html'; ?>
</head>
    <body>
        <div class="form">
            <div class="dropdown">
                <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Menu</button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item disabled">Home</a>
                    <a class="dropdown-item" href="profile-week.php">Week overzicht</a>
                    <a class="dropdown-item" href="logout-sure.php">Uitloggen</a>
                </div>
            </div>
            <h1>Welcome <?php echo $user_name ?></h1>
            <h2>
                <?php
                while($row = $result->fetch_array()){
                    if (!empty($row['jobs_job_name'])) {
                        echo 'jou taak van vandaag is ', $row['jobs_job_name'];
                    } else {
                        echo 'je hebt vandaag geen taak';
                    }
                }
                ?>
            </h2>
        </div>
        <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
        <script src="bootstrap/js/bootstrap.bundle.js" charset="utf-8"></script>
        <script src="js/index.js"></script>
    </body>
</html>
