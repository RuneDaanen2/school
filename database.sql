-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Gegenereerd op: 22 mei 2018 om 12:12
-- Serverversie: 5.6.38
-- PHP-versie: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `your database name`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `jobs`
--

CREATE TABLE `jobs` (
  `job_id` int(11) NOT NULL,
  `job_name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `jobs`
--

INSERT INTO `jobs` (`job_id`, `job_name`) VALUES
(1, 'Tafel dekken'),
(2, 'Tafel afruimen'),
(3, 'Afwasmachine inruimen'),
(4, 'Afwasmachine uitruimen');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `tasks`
--

CREATE TABLE `tasks` (
  `task_id` int(11) NOT NULL,
  `users_user_id` int(11) NOT NULL,
  `users_user_name` varchar(255) NOT NULL,
  `jobs_job_id` int(11) NOT NULL,
  `jobs_job_name` varchar(255) NOT NULL,
  `task_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `tasks`
--

INSERT INTO `tasks` (`task_id`, `users_user_id`, `users_user_name`, `jobs_job_id`, `jobs_job_name`, `task_date`) VALUES
(1, 2, 'sjoerd', 1, 'Tafel dekken', '2018-05-20'),
(2, 3, 'melanie', 2, 'Tafel afruimen', '2018-05-20'),
(3, 4, 'hans', 3, 'Afwasmachine inruimen', '2018-05-20'),
(4, 5, 'marianne', 4, 'Afwasmachine uitruimen', '2018-05-20'),
(5, 6, '906', 0, '', '2018-05-20'),
(6, 7, 'marco', 0, '', '2018-05-20'),
(7, 8, 'femke', 0, '', '2018-05-20'),
(8, 2, 'sjoerd', 0, '', '2018-05-21'),
(9, 3, 'melanie', 1, 'Tafel dekken', '2018-05-21'),
(10, 4, 'hans', 2, 'Tafel afruimen', '2018-05-21'),
(11, 5, 'marianne', 3, 'Afwasmachine inruimen', '2018-05-21'),
(12, 6, '906', 4, 'Afwasmachine uitruimen', '2018-05-21'),
(13, 7, 'marco', 0, '', '2018-05-21'),
(14, 8, 'femke', 0, '', '2018-05-21'),
(15, 2, 'sjoerd', 0, '', '2018-05-22'),
(16, 3, 'melanie', 0, '', '2018-05-22'),
(17, 4, 'hans', 1, 'Tafel dekken', '2018-05-22'),
(18, 5, 'marianne', 2, 'Tafel afruimen', '2018-05-22'),
(19, 6, '906', 3, 'Afwasmachine inruimen', '2018-05-22'),
(20, 7, 'marco', 4, 'Afwasmachine uitruimen', '2018-05-22'),
(21, 8, 'femke', 0, '', '2018-05-22'),
(22, 2, 'sjoerd', 0, '', '2018-05-23'),
(23, 3, 'melanie', 0, '', '2018-05-23'),
(24, 4, 'hans', 0, '', '2018-05-23'),
(25, 5, 'marianne', 1, 'Tafel dekken', '2018-05-23'),
(26, 6, '906', 2, 'Tafel afruimen', '2018-05-23'),
(27, 7, 'marco', 3, 'Afwasmachine inruimen', '2018-05-23'),
(28, 8, 'femke', 4, 'Afwasmachine uitruimen', '2018-05-23'),
(29, 2, 'sjoerd', 4, 'Afwasmachine uitruimen', '2018-05-24'),
(30, 3, 'melanie', 0, '', '2018-05-24'),
(31, 4, 'hans', 0, '', '2018-05-24'),
(32, 5, 'marianne', 0, '', '2018-05-24'),
(33, 6, '906', 1, 'Tafel dekken', '2018-05-24'),
(34, 7, 'marco', 2, 'Tafel afruimen', '2018-05-24'),
(35, 8, 'femke', 3, 'Afwasmachine inruimen', '2018-05-24');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(45) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `user_rank` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_pass`, `user_rank`) VALUES
(2, 'sjoerd', '$2y$10$rAis9/Q7pGz/s3DDT/U36e07ekCEPlX7YbT1F9Ark7INnh/NP.pui', 0),
(3, 'melanie', '$2y$10$2BBapq7Qq1S//e1W4jV/eOeZ7B.JNUKO/tumhoJUOtyDLs7sDSkdK', 0),
(4, 'hans', '$2y$10$0QGjPqHArQLkNkhYHm.O0upgyXOTdoty7sVNX2SuFOKJ7rRCvjY8y', 0),
(5, 'marianne', '$2y$10$w5wK12X6GyIQvTX9.Hpgr.BZzjhJ6fjcOLRxjToyOKzpuqC9g/V8W', 0),
(6, '906', '$2y$10$3K55OPGTl64lcEyZXgwyJO3JhuPuxMIy9TkOWgmRxwU1yukUXH4ku', 1),
(7, 'marco', '$2y$10$o.XjzRZtFRX99t9K8MGclOj265VeGiFsJ.5RUeAJfw5BKFQA5qUWq', 0),
(8, 'femke', '$2y$10$InmZGUfirlmSMkBd49EpjepVwghoqP5TLlEr1bgl5FwqGpB4MCrZ.', 0);

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexen voor tabel `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`task_id`);

--
-- Indexen voor tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `jobs`
--
ALTER TABLE `jobs`
  MODIFY `job_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT voor een tabel `tasks`
--
ALTER TABLE `tasks`
  MODIFY `task_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT voor een tabel `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
