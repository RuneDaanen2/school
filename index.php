<?php
error_reporting(-1);
ini_set('display_errors', 'On');
require 'system/db.php';

?>
<!DOCTYPE html>
<html>
<head>
    <title>Sign-Up/Login Form</title>
    <?php include 'css/css.html'; ?>
</head>
<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    if (isset($_POST['login'])) {
        require 'system/login.php';
    }
}
?>
<body>
    <div class="form">
        <div class="tab-content">
            <div id="login">
                <h1>Mijn beurt</h1>
                <form action="index.php" method="post" autocomplete="off">
                    <div class="field-wrap">
                        <label>Gebruikers naam</label>
                        <input type="text" required autocomplete="off" name="username"/>
                    </div>
                    <div class="field-wrap">
                        <label>Wachtwoord</label>
                        <input type="password" required autocomplete="off" name="password"/>
                    </div>
                    <button class="button button-block" name="login" />Log In</button>
                </form>
            </div>
          <div id="signup"></div>
        </div><!-- tab-content -->
    </div> <!-- /form -->
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="js/index.js"></script>
</body>
</html>
