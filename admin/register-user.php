<?php
error_reporting(-1);
ini_set('display_errors', 'On');

require '../system/db.php';
session_start();

if ( $_SESSION['logged_in-admin'] != 1 ) {
  $_SESSION['message'] = "je moet ingelogt zijn";
  header("location: ../error.php");
}

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    if (isset($_POST['add-user'])) {
        $user_name = $mysqli->escape_string($_POST['username']);
        $password = $mysqli->escape_string($_POST['password']);
        $password = $mysqli->escape_string(password_hash($_POST['password'], PASSWORD_BCRYPT));

        $result = $mysqli->query("SELECT * FROM users WHERE user_name ='$user_name'") or die($mysqli->error());

        if ( $result->num_rows > 0 ) {
            $error = 'naam bestaad all';
        }
        else {
            $sql = "INSERT INTO users (user_name, user_pass) " . "VALUES ('$user_name','$password')";
            if ( $mysqli->query($sql) ){
                header("location: register-user.php");
            }
            else {
                $error = 'er ging iets mis';
            }
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <title>admin gebruiker toevoegen</title>
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <?php include 'css/css.html'; ?>
</head>
    <body>
        <nav class="nav">
            <a class="nav-link" href="week-view.php">Week overzicht</a>
            <a class="nav-link disabled">Voeg gebruiker toe</a>
            <a class="nav-link" href="register-job.php">Voeg taak toe</a>
            <a class="nav-link" href="logout-sure.php">Uitloggen</a>
        </nav>
        <div class="form">
            <div id="login">
                <?php if (!empty($error)) { echo $error;}?>
                <h1>voeg gebruiker toe</h1>
                <form action="register-user.php" method="post" autocomplete="off">
                    <div class="field-wrap">
                        <label>Gebruikers naam</label>
                        <input type="text" required autocomplete="off" name="username"/>
                    </div>
                    <div class="field-wrap">
                        <label>Wachtwoord</label>
                        <input type="password" required autocomplete="off" name="password"/>
                    </div>
                    <button class="button button-block" name="add-user" />voeg toe</button>
                </form>
            </div>
            <br>
            <h2>Alle gebruikers</h2>
            <br>
            <table class="table">
                <thead>
                    <tr>
                        <th style="color: white;" scope="col">#</th>
                        <th style="color: white;" scope="col">gebruikers naam</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $result = $mysqli->query("SELECT * FROM users");
                    $a = 1;
                    while($row = $result->fetch_array())
                    {
                        ?>
                            <tr>
                              <th style="color: white;" scope="row"><?php echo $a++ ?></th>
                              <td style="color: white;"><?php echo $row['user_name'] ?></td>
                            </tr>
                        <?php
                    }
                    ?>
                </tbody>
          </table>
        </div>
    </body>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="../js/index.js"></script>
</html>
