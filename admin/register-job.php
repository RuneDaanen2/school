<?php
/* Displays user information and some useful messages */
require '../system/db.php';
session_start();

if ( $_SESSION['logged_in-admin'] != 1 ) {
  $_SESSION['message'] = "je moet ingelogt zijn";
  header("location: ../error.php");
}
else {
    $user_name = $_SESSION['user_name'];
}
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    if (isset($_POST['add-user'])) {
        $job_name = $mysqli->escape_string($_POST['job_name']);


        $result = $mysqli->query("SELECT * FROM jobs WHERE job_name ='$job_name'") or die($mysqli->error());

        if ( $result->num_rows > 0 ) {
            $error = 'jobname is er al';
        }
        else {
            $sql = "INSERT INTO jobs (job_name) " . "VALUES ('$job_name')";
            if ( $mysqli->query($sql) ){
                header("location: register-job.php");
            }
            else {
                $error = 'er ging iets mis';
            }
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <title><?= $user_name ?></title>
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <?php include 'css/css.html'; ?>
</head>
    <body>
        <nav class="nav">
            <a class="nav-link" href="week-view.php">Week overzicht</a>
            <a class="nav-link" href="register-user.php">Voeg gebruiker toe</a>
            <a class="nav-link disabled">Voeg taak toe</a>
            <a class="nav-link" href="logout-sure.php">Uitloggen</a>
        </nav>
        <div>
        <div class="form">
            <div id="login">
                <h1>Voeg een taak toe</h1>
                <form action="register-job.php" method="post" autocomplete="off">
                    <div class="field-wrap">
                        <label>Taak</label>
                        <input type="text" required autocomplete="off" name="job_name"/>
                    </div>
                    <button class="button button-block" name="add-user" />Voeg toe</button>
                </form>
            </div>
            <br>
            <h2>Alle taken</h2>
            <br>
            <table class="table">
                <thead>
                    <tr>
                        <th style="color: white;" scope="col">#</th>
                        <th style="color: white;" scope="col">Taak</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $result = $mysqli->query("SELECT * FROM jobs");
                    $a = 1;
                    while($row = $result->fetch_array())
                    {
                        ?>
                            <tr>
                              <th style="color: white;" scope="row"><?php echo $a++ ?></th>
                              <td style="color: white;"><?php echo $row['job_name'] ?></td>
                            </tr>
                        <?php
                    }
                    ?>
                </tbody>
          </table>
        </div>
        </div>
    </body>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="../js/index.js"></script>
</html>
