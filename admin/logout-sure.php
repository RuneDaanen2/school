<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>logout</title>
    <?php include 'css/css.html'; ?>
</head>

<body>
    <div class="form">
          <h1>Weet je het zeker dat je wil uitloggen?</h1>
          <a href="../logout.php"><button class="button button-block"/>ja</button></a>
          <br>
          <a href="week-view.php"><button class="button button-block"/>nee</button></a>
    </div>
</body>
</html>
